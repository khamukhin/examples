# Implemented for you
# Call that helper and aggregate+print the results as at the top
def get_stacktraces():
    return [
        ["main", "workloop", "select"],
        ["main","parse_args"],
        ["main", "workloop", "parse_data", "parse_entry"],
        ["main", "workloop", "select"]
    ]


# Function call node object
# uses dict (hash) structure for children
# it helps to find if we already have given children
# first approach was using list for children, but then we had to iterate whole list each time
class FuncCall(object):
    def __init__(self, name):
        self.name = name
        self.calls = 1
        self.children = dict()
    
    def add_call(self, name):
        if name in self.children:
            self.children[name].calls += 1
        else:
            new_node = FuncCall(name)
            self.children[name] = new_node

        return self.children[name]


# aggregate and print stack traces
def pretty_print(stack):
    head = FuncCall('head')

    # aggregate
    for line in stack:
        pointer = head
        for func in line:
            pointer = pointer.add_call(func)

    # output
    # recursively outputs given nodes
    # for each nodes outputs its children
    # before printing children it sorting it by number of calls
    def print_nodes(nodes, level=0):
        tab_size = 4
        indent = " " * tab_size * level

        for node in nodes:
            print "%s%s %s" % (indent, node.calls, node.name)
            print_nodes(sorted(node.children.values(), key=lambda x: x.calls, reverse=True), level+1)

    print_nodes(head.children.values())


# example input
#
# 4 main
#     3 workloop
#         2 select
#         1 parse_data
#             1 parse_entry
#     1 parse_args
pretty_print(get_stacktraces())


# empty input
#
pretty_print([])


# multiple head funcs
#
# 1 test
#     1 foo
# 1 main
# 1 root
pretty_print([['main'], ['root'], ['test', 'foo']])


# same name on different levels
#
# 2 main
#     1 main
#     1 foo
# 1 foo
#     1 foo
pretty_print([['main', 'foo'], ['foo', 'foo'], ['main', 'main']])
