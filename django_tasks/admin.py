from django.contrib import admin
from django.db.models import Count, Max, Min
from django.utils import timezone

from models import TaskResult, ScheduledTask, put_task


class TaskResultAdmin(admin.ModelAdmin):
    list_display = ('name', 'next_run', 'last_run', 'duration', 'is_success', 'is_running')
    actions = ('run',)

    # Disabled for a while
    def queryset_(self, request):
        names = self.model.objects.values('name').annotate(Count('name'), Max('id'))
        ids = [x['id__max'] for x in names]
        return self.model.objects.filter(id__in=ids)

    def next_run(self, obj):
        scheduled = ScheduledTask.objects.filter(name=obj.name, is_running=False).annotate(Min('run_at'))[:1]
        if scheduled:
            return scheduled[0].run_at

    def is_running(self, obj):
        scheduled = ScheduledTask.objects.filter(name=obj.name, is_running=True).annotate(Min('run_at'))[:1]
        if scheduled:
            return 'Running'
        else:
            return ''

    def run(self, request, queryset):
        now = timezone.now()
        kwargs = {'run_at': now}
        for obj in queryset:
            put_task(obj.name, **kwargs)
        self.message_user(request, "Tasks will be started now.")

admin.site.register(TaskResult, TaskResultAdmin)