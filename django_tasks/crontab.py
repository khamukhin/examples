# -*- coding: utf-8 -*-
# from __future__ import absolute_import
from tasks.manager import TaskManager


manager = TaskManager()

manager.cron('0 2 * * *', 'parser.management.commands.offer.run')
manager.cron('0 3 * * *', 'parser.management.commands.offer_photo.run')
manager.cron('0 */3 * * *', 'parser.management.commands.offer_url.run')

manager.on('offers-ready', 'parser.management.commands.cleanup.run')