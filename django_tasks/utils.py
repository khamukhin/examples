import os
import sys
import inspect
import traceback
from pickle import dumps
from subprocess import Popen, PIPE
from threading import Thread
from time import sleep, time

from django.utils import timezone


def get_func(name):
    package, _, name = name.rpartition('.')
    __import__(package)
    try:
        func = getattr(sys.modules[package], name)
        return getattr(func, 'main', func)
    except AttributeError:
        package = '{}.{}'.format(package, name)
        name = 'main'
        __import__(package)
        return getattr(sys.modules[package], name)


def format_call(func_name, args, only_args=False):
    try:
        func = get_func(func_name)
    except Exception:
        argstr = '{}'.format(args)
    else:
        spec = inspect.getargspec(func)
        for arg, value in zip(spec.args[::-1], (spec.defaults or [])[::-1]):
            if arg not in args:
                args[arg] = value

        argstr = u', '.join(u'{}={}'.format(r, args.get(r, 'Unknown')) for r in spec.args)

    if only_args:
        return argstr
    else:
        return u'{}({})'.format(func_name, argstr)


class TaskBreaker(object):
    def __init__(self):
        self._break = False

    def fire(self):
        self._break = True

    def wait(self, timeout):
        t = time()
        while not self._break and time() - t < timeout:
            sleep(0.1)

        return self._break

    def __nonzero__(self):
        return self._break


class TaskWrapper(object):
    def __init__(self, task):
        self.task = task
        self.will_be_killed = False

    def __str__(self):
        return format_call(self.task.name, self.task.args)

    def run(self, on_job_done=None):
        manage = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'manage.py')
        cmd = [sys.executable, manage, 'execute', '--traceback', self.task.name,
               format_call(self.task.name, self.task.args, True).encode('utf8')]
        try:
            process = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        except Exception as e:
            tb = traceback.format_exc()
            self.task.done(False, u'Task {} execution failed({}): {}'.format(
                self.task, e, tb), timezone.now())

            return False

        self.process = process
        self.on_job_done = on_job_done
        self.started_at = timezone.now()
        self.thread = Thread(target=self._run)
        self.thread.start()
        return True

    def _run(self):
        out, err = self.process.communicate(dumps(self.task.args, 2))

        log = out
        if err:
            if log:
                log += '\n'

            log += err

        is_success = len(err) == 0 and self.process.returncode == 0

        if self.on_job_done:
            self.on_job_done(self, is_success, log)

        self.task.done(is_success, log, self.started_at)

    def join(self):
        self.thread.join()

    def kill(self):
        self.will_be_killed = True
        self.process.terminate()
