# -*- coding: utf-8 -*-
"""Tasks package.

Allows register and run periodic tasks using cron syntax. All results stored and could be viewed at admin page.

See crontab.py for an example of periodic tasks registering.

"""
from __future__ import absolute_import
from collections import OrderedDict
from datetime import datetime, timedelta
import logging

from crontab import CronTab
from django.utils import timezone

from tasks.models import put_task


logger = logging.getLogger('task_executor')


class Task(object):
    def __init__(self, name):
        self.name = name

    def put(self, *args, **kwargs):
        put_task(self.name, *args, **kwargs)


class TaskManager(object):
    def __init__(self):
        self.tasks = OrderedDict()
        self.schedule = {}
        self.events = {}
        self._current_tasks = []

    def task(self, task, name=None):
        """Registers task for later manual start."""
        task = Task(task)
        if name:
            task.name = name
        return self._append_task(task)

    def cron(self, at, task, **kwargs):
        """Registers task to scheduled and periodic start."""
        task = Task(task)
        self._append_task(task, True)
        self.schedule.setdefault(task.name, []).append((CronTab(at), kwargs))
        return task

    def on(self, event, task, **kwargs):
        """Runs task on given event fires."""
        task = Task(task)
        self.events.setdefault(event, []).append((task, kwargs))
        self._append_task(task, True)
        return task

    def emit(self, event, *args, **kwargs):
        """Fires event."""
        handlers = self.events.get(event)
        if not handlers:
            logger.error('There are no any handlers for "%s" event', event)
            return

        for task, params in handlers:
            params = params.copy()
            for k, v in params.iteritems():
                if callable(v):
                    params[k] = v(*args, **kwargs)

            task.put(**params)

    def get_periodic_tasks(self, now=None):
        """Lists tasks with schedule."""
        now = now or timezone.now()
        for name, crons in self.schedule.iteritems():
            for cron, kwargs in crons:
                yield self.tasks[name], now + timedelta(seconds=cron.next(timezone.localtime(now))), kwargs

    def put(self, name, *args, **kwargs):
        """Puts task next run to schedule."""
        task = self._get_task(name)
        if not task:
            raise Exception('Task [%s] is undefined')

        task.put(*args, **kwargs)

    def _get_task(self, name):
        try:
            return self.tasks[name]
        except KeyError:
            return None

    def _append_task(self, task, skip_if_exists=False):
        if not task.name:
            Exception('Task must have name' % task.name)

        if task.name in self.tasks:
            if skip_if_exists:
                return task
            else:
                raise Exception('Task [%s] already exists' % task.name)

        self.tasks[task.name] = task
        return task