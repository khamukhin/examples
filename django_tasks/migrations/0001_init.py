# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ScheduledTask'
        db.create_table(u'tasks_scheduledtask', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('args', self.gf('picklefield.fields.PickledObjectField')()),
            ('run_at', self.gf('django.db.models.fields.DateTimeField')()),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=10)),
            ('is_running', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'tasks', ['ScheduledTask'])

        # Adding unique constraint on 'ScheduledTask', fields ['key', 'run_at']
        db.create_unique(u'tasks_scheduledtask', ['key', 'run_at'])

        # Adding model 'TaskResult'
        db.create_table(u'tasks_taskresult', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('args', self.gf('picklefield.fields.PickledObjectField')()),
            ('last_run', self.gf('django.db.models.fields.DateTimeField')()),
            ('is_success', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('log', self.gf('django.db.models.fields.TextField')()),
            ('duration', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'tasks', ['TaskResult'])


    def backwards(self, orm):
        # Removing unique constraint on 'ScheduledTask', fields ['key', 'run_at']
        db.delete_unique(u'tasks_scheduledtask', ['key', 'run_at'])

        # Deleting model 'ScheduledTask'
        db.delete_table(u'tasks_scheduledtask')

        # Deleting model 'TaskResult'
        db.delete_table(u'tasks_taskresult')


    models = {
        u'tasks.scheduledtask': {
            'Meta': {'unique_together': "(('key', 'run_at'),)", 'object_name': 'ScheduledTask'},
            'args': ('picklefield.fields.PickledObjectField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_running': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'run_at': ('django.db.models.fields.DateTimeField', [], {})
        },
        u'tasks.taskresult': {
            'Meta': {'object_name': 'TaskResult'},
            'args': ('picklefield.fields.PickledObjectField', [], {}),
            'duration': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_success': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_run': ('django.db.models.fields.DateTimeField', [], {}),
            'log': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['tasks']