# -*- coding: utf-8 -*-
import sys
from pickle import loads

from django.core.management.base import BaseCommand

from tasks.utils import get_func


class Command(BaseCommand):
    def handle(self, *args, **options):
        func_args = loads(sys.stdin.read())
        func = get_func(args[0])
        func(**func_args)