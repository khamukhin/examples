# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    def handle(self, *args, **options):
        from tasks.crontab import manager

        for task, start, kwargs in manager.get_periodic_tasks():
            kwargs = kwargs.copy()
            for k, v in kwargs.iteritems():
                if callable(v):
                    kwargs[k] = v(start)

            manager.put(task.name, run_at=start, **kwargs)