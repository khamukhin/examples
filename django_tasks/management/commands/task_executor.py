# -*- coding: utf-8 -*-
import time
import signal
import logging
from threading import Lock
from datetime import datetime
from optparse import make_option

from django.core.management.base import BaseCommand

from tasks.models import ScheduledTask, TaskResult


logger = logging.getLogger('task_executor')


class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('-s', '--pool-size', type=int, default=2, dest='pool_size'),
        make_option('-l', '--log', dest='log'),
        make_option('-d', '--dump', action='store_true', default=False, dest='dump'),
        make_option('-f', '--flush', action='store_true', default=False, dest='flush'),
        make_option('-r', '--results', action='store_true', default=False, dest='results'))

    def handle(self, *args, **options):
        log_opts = dict(format='%(asctime)s %(levelname)s %(message)s', level=logging.INFO)
        if options['log']:
            log_opts['filename'] = options['log']

        logging.basicConfig(**log_opts)

        if options['dump']:
            dump()
        elif options['flush']:
            flush()
        elif options['results']:
            results()
        else:
            main(options['pool_size'])


def main(pool_size):
    from tasks.models import pop_task
    from tasks.utils import TaskBreaker, TaskWrapper

    jobs = []
    breaker = TaskBreaker()
    job_lock = Lock()

    def task_done(job, is_success, log):
        with job_lock:
            jobs.remove(job)
            if job.will_be_killed:
                logger.info(u'Task killed {}'.format(job.task))
            else:
                logger.info(u'Task done {}'.format(job.task))

    def handler(signum, frame):
        breaker.fire()

    signal.signal(signal.SIGTERM, handler)
    signal.signal(signal.SIGINT, handler)

    def enqueue_task(task):
        job = TaskWrapper(task)
        if job.run(task_done):
            logger.info(u'Run task {}'.format(task))
            jobs.append(job)
        else:
            logger.info(u'Task start failed {}'.format(task))

    try:
        while not breaker:
            task, _ = pop_task([r.task.key for r in jobs], len(jobs) >= pool_size)

            if task:
                enqueue_task(task)
            else:
                breaker.wait(5)
    except Exception:
        logger.exception('Task executor problem')
        breaker.fire()

    while jobs:
        time.sleep(1)


def dump():
    for r in ScheduledTask.objects.filter(run_at__lte=datetime.now()).order_by('run_at'):
        print r


def flush():
    ScheduledTask.objects.filter(run_at__lte=datetime.now()).delete()


def results():
    for r in TaskResult.objects.all().order_by('last_run')[:5]:
        print '>>>>>>', r
        print r.log
