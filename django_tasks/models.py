from django.db import models, IntegrityError, transaction
from django.utils import timezone
from picklefield import PickledObjectField
import hashlib

from tasks.utils import format_call


PRIORITY_HIGH = 0
PRIORITY_NORMAL = 10
PRIORITY_IDLE = 100


class ScheduledTask(models.Model):
    class Meta:
        unique_together = ('key', 'run_at',)

    key = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    args = PickledObjectField()
    run_at = models.DateTimeField()
    priority = models.IntegerField(default=PRIORITY_NORMAL)
    is_running = models.BooleanField(default=False)

    def run(self):
        self.is_running = True
        self.save()

    def done(self, is_success, log, start_at, end_at=None, real_args=None, remove_schedule=True):
        result = TaskResult(name=self.name, args=real_args or self.args, last_run=start_at, is_success=is_success,
                            log=log, duration=round(((end_at or timezone.now()) - start_at).total_seconds()))
        result.save()

        if remove_schedule:
            self.delete()

    def __unicode__(self):
        return u'#{} {}'.format(self.id, format_call(self.name, self.args))


class TaskResult(models.Model):
    name = models.CharField(max_length=255)
    args = PickledObjectField()
    last_run = models.DateTimeField()
    is_success = models.BooleanField()
    log = models.TextField()
    duration = models.IntegerField()

    def __unicode__(self):
        return u'{} {} at {}'.format(format_call(self.name, self.args), ['fail', 'ok'][self.is_success], self.last_run)


def make_key(name, args):
    excluded_args = ['title']
    return hashlib.md5(
        name + repr(sorted(filter(lambda arg: arg[0] not in excluded_args, args.iteritems())))).hexdigest()


def put_task(name, *args, **kwargs):
    run_at = kwargs.pop('run_at', None) or timezone.now()
    key = kwargs.pop('task_key', None) or make_key(name, kwargs)
    priority = kwargs.pop('priority', PRIORITY_NORMAL)

    task = ScheduledTask(key=key, name=name, args=kwargs, run_at=run_at, priority=priority)

    try:
        sid = transaction.savepoint()
        task.save()
        transaction.savepoint_commit(sid)
    except IntegrityError:
        transaction.savepoint_rollback(sid)


def pop_task(keys=None, pool_is_full=False, now=None):
    now = now or timezone.now()

    M = ScheduledTask
    if keys:
        tasks = M.objects.filter(is_running=False, run_at__lte=now, key__in=keys).order_by('priority', 'run_at').all()

        similar_tasks = {r.key: r for r in tasks}.values()
        for t in tasks[:]:
            if t not in similar_tasks:
                t.delete()
    else:
        similar_tasks = []

    if similar_tasks:
        return None, similar_tasks

    task = None
    if not pool_is_full:
        task = M.objects.filter(run_at__lte=now, is_running=False).order_by('priority', 'run_at')[:1]
        if task:
            task = task[0]
            task.run()
        else:
            task = None

    return task, []
