"""
Merge of logfiles
"""
from heapq import heapify, heappop, heappush, merge as heapmerge
from operator import itemgetter
from optparse import OptionParser, make_option
import os
import re

import sys
from datetime import datetime
import itertools
from filter import filter_

DATE_FORMAT = '%a %b %d %H:%M:%S %Y'


def merge_files(level, *input_files):
    open_files = []
    try:
        for f in input_files:
            open_files.append(filter_(open(f, 'r'), level=level, return_split=True))
            #open_files.append(open(f, 'r'))

        return merge(level, *open_files)

    finally:
        for f in open_files:
            f.close()


def merge(level, *inputs):
    def parse_line(line, i):
        parts = re.findall(r'\[(.*?)\]', line)
        return datetime.strptime(parts[0], DATE_FORMAT), i, parts[1], line

    buffer = []
    open_files = list(inputs)


    #for line in sorted(itertools.chain(*open_files)):
    #    print line[2].rstrip()

    for line in heapmerge(*open_files):
        print line[2].rstrip()
    return


    while len(open_files):

        last_inserted = None

        heap = []

        min_threshold = None

        def get_next(func):
            for i, f in enumerate(open_files):
                try:
                    for x in range(1000):
                        line = next(f)
                        min_threshold = line[0]
                        func(line)

                except StopIteration:
                    f.close()
                    open_files.pop(i)
                    break
            return min_threshold

        def heappush_(val):
            heappush(heap, val)

        min_threshold = get_next(heap.append)
        heapify(heap)



        #def next():
        #    cf, ct, cp, cr, it = heappop(heap)
        #    for f, t, p, r in it:
        #        heappush(heap, (f, t, p, r, it))
        #        break
        #
        #    return cf, ct, cp, cr, it

        while heap:
            line = heappop(heap)
            if line[0] > min_threshold:
                print >> sys.stderr, min_threshold
                heappush(heap, line)
                min_threshold = get_next(heappush_)
            else:
                print line[2].rstrip()



def merge_main():
    level, files = merge_args()
    merge_files(level, *files)


def merge_args():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage, option_list=[
        make_option("--level", dest="level", default='debug', help="Filter log to critically level"),
    ])

    (opts, args) = parser.parse_args()

    files = []
    for input_file in args:
        if os.path.isdir(input_file):
            files += [os.path.join(input_file, f) for f in os.listdir(input_file) if os.path.isfile(os.path.join(input_file, f))]
        elif os.path.isfile(input_file):
            files.append(input_file)
        else:
            parser.error('No such file: {}'.format(input_file))

    if len(files) < 2:
        parser.print_help()
        sys.exit(1)

    return opts.level, files


if __name__ == '__main__':
    merge_main()