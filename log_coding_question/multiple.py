#!/usr/bin/python
"""
Runs multiple merges, see merge.py docs

Usage example: python multiple.py --level info /dir/to/many/logs
"""

import sys
import subprocess

from filter import filter_
from merge import MERGE_FUNC, merge_args

POOL_SIZE = 4


def multiple_main():
    level, files = merge_args()

    cmd = [sys.executable, 'merge.py', '--level', level]

    procs_to_run = POOL_SIZE
    files_per_proc = len(files) / POOL_SIZE

    if files_per_proc < 2:
        procs_to_run = 1

    procs = []

    for i in range(procs_to_run):
        proc_files = []
        if i == procs_to_run - 1:
            proc_files = files
        else:
            for x in range(files_per_proc):
                proc_files.append(files.pop())

        procs.append(subprocess.Popen(cmd + proc_files, stdout=subprocess.PIPE))

    MERGE_FUNC(*[filter_(p.stdout, return_split=True) for p in procs])


if __name__ == '__main__':
    multiple_main()