#!/usr/bin/python
"""
Merge logs

Merge two or more logs respecting chronological order
It's possible to filter log lines be level, see filter.py docs
Usage example: python merge.py logfile1 logfile2
Usage example: python merge.py --level error dir/to/logs
"""
from itertools import islice

import os
import sys
import re
from operator import itemgetter
from optparse import OptionParser, make_option
from heapq import heapify, heappop, heappush, merge as heapmerge

from filter import filter_

HEAP_CHUNK = 10


def merge(*inputs):
    buffer = []
    active_inputs = list(inputs)

    while len(active_inputs):
        if not buffer:
            for i, f in enumerate(active_inputs):
                try:
                    line = next(f)
                except StopIteration:
                    active_inputs.pop(i)
                    continue

                buffer.append(line + (i,))
        else:
            buffer.sort(key=itemgetter(0))
            _, _, output, input_idx = buffer.pop(0)
            print output.rstrip()

            try:
                line = next(active_inputs[input_idx])
                if line:
                    buffer.append(line + (input_idx,))
            except (StopIteration, IndexError):
                continue


def merge_heap(*inputs):
    active_inputs = list(inputs)

    heap = []

    def get_next(func):
        line = None
        get_next.min_threshold = None
        for i, f in enumerate(active_inputs):
            try:
                for x in xrange(HEAP_CHUNK):
                    line = next(f)
                    func(line)

            except StopIteration:
                active_inputs.pop(i)
            if line:
                if not get_next.min_threshold or get_next.min_threshold < line[0]:
                    get_next.min_threshold = line[0]

    def heappush_(val):
        heappush(heap, val)

    get_next(heap.append)
    heapify(heap)

    while heap:
        timestamp, _, output = heappop(heap)
        if get_next.min_threshold and timestamp >= get_next.min_threshold:
            heappush(heap, (timestamp, _, output))
            get_next(heappush_)
        else:
            print output.rstrip()


def merge_heapmerge(*inputs):
    for line in heapmerge(*inputs):
        print line[2].rstrip()

MERGE_FUNC = merge_heapmerge


def merge_files(level, *input_files):
    opened = []
    try:
        for f in input_files:
            opened.append(filter_(open(f, 'r'), level=level, return_split=True))
        return MERGE_FUNC(*opened)
    finally:
        for f in opened:
            f.close()


def merge_args():
    usage = "usage: %prog --level <level> <filename1> <filename2> [<filename3...]"
    parser = OptionParser(usage, option_list=[
        make_option("--level", dest="level", default='debug', help="Filter log to critically level"),
    ])

    opts, args = parser.parse_args()

    input_files = []
    for f in args:
        if os.path.isdir(f):
            input_files += [os.path.join(f, x) for x in os.listdir(f) if os.path.isfile(os.path.join(f, x))]
        elif os.path.isfile(f):
            input_files.append(f)
        else:
            parser.error('No such file: {}'.format(f))

    if len(input_files) < 2:
        parser.print_help()
        sys.exit(1)

    return opts.level, input_files


def merge_main():
    level, files = merge_args()
    merge_files(level, *files)


if __name__ == '__main__':
    merge_main()