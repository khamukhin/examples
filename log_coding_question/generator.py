#!/usr/bin/python
"""
Generator of log files

Usage example: seq 1 10 | xargs -P 10 -I{} sh -c "python generator.py --lines 10000000 > test_{}.log"
"""

from random import randrange, choice
from optparse import OptionParser, make_option
from datetime import datetime, timedelta

DATE_FORMAT = '%a %b %d %H:%M:%S %Y'

LEVELS = ('debug', 'info', 'warning', 'error', 'critical')

LOG_MESSAGES = ('client denied by server configuration: /export/home/live/test',)

MAX_TIMESTAMP_STEP = 10000


def generator_main():
    usage = "usage: %prog [options]"
    parser = OptionParser(usage, option_list=[
        make_option("--lines", type="int", dest="lines", default=1000, help="Lines to write to log"),
        make_option('--start', dest='start', type='string', default='2013-10-14',
                    help='Start date in YYYY-MM-DD format'),
    ])

    opts, args = parser.parse_args()

    lines = opts.lines
    start = datetime.strptime(opts.start, '%Y-%m-%d')

    while lines > 0:
        lines -= 1
        start = start + timedelta(milliseconds=randrange(0, MAX_TIMESTAMP_STEP))
        start_string = start.strftime(DATE_FORMAT)
        print '[{}] [{}] [client 127.0.0.1] {}'.format(start_string, choice(LEVELS), choice(LOG_MESSAGES))

if __name__ == '__main__':
    generator_main()