#!/usr/bin/python
"""
Test stdin logs chronological order

Usage example: ./merge.py ../yandex_logs_10000 | python test.py
"""

import sys
from datetime import datetime

from generator import DATE_FORMAT


def parse_timestamp(line):
    parts = line.split('] ', 1)
    return datetime.strptime(parts[0].lstrip(' ['), DATE_FORMAT)


def test_main():
    prev = None
    for line in sys.stdin:
        timestamp = parse_timestamp(line)
        if prev:
            assert prev <= timestamp
        prev = timestamp


if __name__ == '__main__':
    test_main()