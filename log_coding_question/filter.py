#!/usr/bin/python
"""
Filter logfile by level

Log format: [timestamp] [level] [source] message
Levels: 'debug', 'info', 'warning', 'error', 'critical'
Usage example: python filter.py --level warning ../yandex_logs_10000/1.log
"""

import os
import sys
from optparse import make_option, OptionParser
from datetime import datetime

from generator import DATE_FORMAT, LEVELS


class CorruptedLogLine(Exception):
    pass


def filter_(input_file, level=LEVELS[0], return_split=False, ignore_corrupted=True):
    def split_line(line):
        parts = line.split('] ')
        if len(parts) < 4:
            raise CorruptedLogLine
        return datetime.strptime(parts[0].lstrip(' ['), DATE_FORMAT), parts[1].lstrip(' ['), line

    levels = {x: i for i, x in enumerate(LEVELS)}

    for line in input_file:
        try:
            line = split_line(line)
        except CorruptedLogLine:
            if ignore_corrupted:
                continue
            else:
                raise
        if levels[level] <= levels[line[1]]:
            yield line if return_split else line[2]


def filter_args():
    usage = "usage: %prog --level <level> <filename>"
    parser = OptionParser(usage, option_list=[
        make_option("--level", dest="level", default='debug', help="Filter log to critically level"),
    ])

    opts, args = parser.parse_args()

    if opts.level not in LEVELS or len(args) != 1:
        parser.print_help()
        sys.exit(1)

    if not os.path.isfile(args[0]):
        parser.error('No such file: {}'.format(args[0]))
        sys.exit(1)

    return opts.level, args[0]


def filter_main():
    level, input_file = filter_args()
    with open(input_file, 'r') as f:
        for line in filter_(f, level):
            print line.rstrip()


if __name__ == '__main__':
    filter_main()